# Joplin-Helper

Little widget to make the use of joplin even more comfortable

## nasin pali

ni li ilo sona pi jan nasa en jan piko. 
ni li pana e pona tawa ilo sona Jopilin li open e poki toki li pana e toki ni tawa ilo Jopilin. 

o kama sona e toki ID e toki Token. ni li lon e poki pi lawa wile. o pana e toki ID e toki Token lon ma pona pi toki ni. o pali e nasin lili pi ilo sitelen.

ni li pona kepeken nasin Ketin Tinki Tone.

## Description
Opens a window with input area which is pasted into a Joplin note specified in the .sh script. Works wonderfully with a keyboard shortcut as a stuff collector for the Getting Things Done method.

## Installation
Find out the token and id of the note you want to paste into; it's somewhere in the options of Joplin. Put it in the script.

## Authors and acknowledgment
Many thanks and love to mad!

## License
GPL

## Project status
Done.

