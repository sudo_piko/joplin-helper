#!/bin/bash

set -e
set -o pipefail

token=verylongtokenhere

id=slightlyshorteridhere

text=$(curl -s http://127.0.0.1:41184/notes/$id\?token=$token\&fields=body | jq -r .body)

neu=$(zenity --entry --title Joplinhelper --text "Insert Todo")


gesamt="$neu

$text"


gejsont=$(echo "$gesamt" | jq -R -r -s tojson)

curl -s -X PUT --data "{ \"body\" : $gejsont }" http://127.0.0.1:41184/notes/$id\?token=$token >/dev/null
